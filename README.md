[![pipeline status](https://gitlab.com/nubodev/ade/badges/master/pipeline.svg)](https://gitlab.com/nubodev/ade/-/commits/master)

# containers

Nubonetics containers

# Google

[![Run on Google Cloud](https://storage.googleapis.com/cloudrun/button.svg)](https://console.cloud.google.com/cloudshell/editor?shellonly=true&cloudshell_image=gcr.io/cloudrun/button&cloudshell_git_repo=https://gitlab.com/nubodev/ade.git)